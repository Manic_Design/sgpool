<?php 
ob_start();
?>
<?php
  global $current_page;
  $current_page = "page-museum-details";
  $page_title = "Museum";
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
  <?php include 'includes/head.php'; ?>
</head>

<body class="<?php echo $current_page; ?>"> <!-- home-expand-header-version -->
  <!-- only the home page has the class home-expand-header-version by default -->

  <?php include "includes/preloader.php"; ?>

  <?php include "includes/header_desktop.php"; ?>
  <?php include "includes/header_mobile.php"; ?>

  <div class="header-desktop-spacer hidden-xs hidden-sm"></div>
  <div class="header-mobile-spacer visible-xs visible-sm"></div>

  <div id="museum-detail-sticky-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h3>Posters From The Past</h3>
          <a href="museum.html" class="back-cta"><span>Back</span></a>
        </div>
      </div>    
    </div>
  </div>

  <div id="page-wrapper">
    <div id="page-wrapper-content">
      
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            
            <div id="page-museum-details-item-container">

              <div id="item-1" class="page-museum-details-item">

                <div class="scroll-target" data-value="1960s"></div>
            
                <div class="page-museum-detail-item-title">
                  <h2>1960s</h2>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, <br class="hidden-xs hidden-sm"> sed do eiusmod tempor.</p> -->
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="posters-masonry-container-01" class="posters-masonry-container-01 masonry-container">
                    <div class="item" data-width="600" data-height="1379">
                      <img src="images_cms/museum/posters/1960s/01.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="788">
                      <img src="images_cms/museum/posters/1960s/02.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="786">
                      <img src="images_cms/museum/posters/1960s/03.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="894">
                      <img src="images_cms/museum/posters/1960s/04.jpg">
                    </div>
                  </div>                  
                </div>

              </div> <!-- end page-museum-details-item -->
              
              <div id="item-2" class="page-museum-details-item">

                <div class="scroll-target" data-value="1970s"></div>
            
                <div class="page-museum-detail-item-title">
                  <h2>1970s</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="posters-masonry-container-02" class="posters-masonry-container-02 masonry-container">
                    <div class="item" data-width="600" data-height="746">
                      <img src="images_cms/museum/posters/1970s/01.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="822">
                      <img src="images_cms/museum/posters/1970s/02.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="833">
                      <img src="images_cms/museum/posters/1970s/03.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="840">
                      <img src="images_cms/museum/posters/1970s/04.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="789">
                      <img src="images_cms/museum/posters/1970s/05.jpg">
                    </div>                  
                    <div class="item" data-width="600" data-height="799">
                      <img src="images_cms/museum/posters/1970s/06.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="812">
                      <img src="images_cms/museum/posters/1970s/07.jpg">
                    </div>                    
                    <div class="item" data-width="600" data-height="804">
                      <img src="images_cms/museum/posters/1970s/08.jpg">
                    </div>                    
                    <div class="item" data-width="600" data-height="801">
                      <img src="images_cms/museum/posters/1970s/09.jpg">
                    </div>                    
                    <div class="item" data-width="600" data-height="786">
                      <img src="images_cms/museum/posters/1970s/10.jpg">
                    </div>                    
                    <div class="item" data-width="600" data-height="801">
                      <img src="images_cms/museum/posters/1970s/11.jpg">
                    </div>                    
                    <div class="item" data-width="600" data-height="789">
                      <img src="images_cms/museum/posters/1970s/12.jpg">
                    </div>                    
                    <div class="item" data-width="600" data-height="860">
                      <img src="images_cms/museum/posters/1970s/13.jpg">
                    </div>                    
                  </div>
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-3" class="page-museum-details-item">

                <div class="scroll-target" data-value="1980s"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>1980s</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="posters-masonry-container-03" class="posters-masonry-container-03 masonry-container">
                    <div class="item" data-width="600" data-height="807">
                      <img src="images_cms/museum/posters/1980s/01.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="831">
                      <img src="images_cms/museum/posters/1980s/02.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="960">
                      <img src="images_cms/museum/posters/1980s/03.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="951">
                      <img src="images_cms/museum/posters/1980s/04.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="959">
                      <img src="images_cms/museum/posters/1980s/05.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="957">
                      <img src="images_cms/museum/posters/1980s/06.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="957">
                      <img src="images_cms/museum/posters/1980s/07.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="959">
                      <img src="images_cms/museum/posters/1980s/08.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="960">
                      <img src="images_cms/museum/posters/1980s/09.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="960">
                      <img src="images_cms/museum/posters/1980s/10.jpg">
                    </div>                

                    <div class="item" data-width="600" data-height="963">
                      <img src="images_cms/museum/posters/1980s/11.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="955">
                      <img src="images_cms/museum/posters/1980s/12.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="957">
                      <img src="images_cms/museum/posters/1980s/13.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="951">
                      <img src="images_cms/museum/posters/1980s/14.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="960">
                      <img src="images_cms/museum/posters/1980s/15.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="945">
                      <img src="images_cms/museum/posters/1980s/16.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="957">
                      <img src="images_cms/museum/posters/1980s/17.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="960">
                      <img src="images_cms/museum/posters/1980s/18.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="952">
                      <img src="images_cms/museum/posters/1980s/19.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="942">
                      <img src="images_cms/museum/posters/1980s/20.jpg">
                    </div>

                    <div class="item" data-width="600" data-height="925">
                      <img src="images_cms/museum/posters/1980s/21.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="921">
                      <img src="images_cms/museum/posters/1980s/22.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="925">
                      <img src="images_cms/museum/posters/1980s/23.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="925">
                      <img src="images_cms/museum/posters/1980s/24.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="886">
                      <img src="images_cms/museum/posters/1980s/25.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="889">
                      <img src="images_cms/museum/posters/1980s/26.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="931">
                      <img src="images_cms/museum/posters/1980s/27.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="881">
                      <img src="images_cms/museum/posters/1980s/28.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="889">
                      <img src="images_cms/museum/posters/1980s/29.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="927">
                      <img src="images_cms/museum/posters/1980s/30.jpg">
                    </div>

                    <div class="item" data-width="600" data-height="914">
                      <img src="images_cms/museum/posters/1980s/31.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="882">
                      <img src="images_cms/museum/posters/1980s/32.jpg">
                    </div>
                    <!-- <div class="item" data-width="600" data-height="947">
                      <img src="images_cms/museum/posters/1980s/33.jpg">
                    </div> -->
                    <div class="item" data-width="600" data-height="941">
                      <img src="images_cms/museum/posters/1980s/34.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="947">
                      <img src="images_cms/museum/posters/1980s/35.jpg">
                    </div>
                    <!-- <div class="item" data-width="600" data-height="949">
                      <img src="images_cms/museum/posters/1980s/36.jpg">
                    </div> -->
                    <div class="item" data-width="600" data-height="946">
                      <img src="images_cms/museum/posters/1980s/37.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="947">
                      <img src="images_cms/museum/posters/1980s/38.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="947">
                      <img src="images_cms/museum/posters/1980s/39.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="940">
                      <img src="images_cms/museum/posters/1980s/40.jpg">
                    </div>

                    <div class="item" data-width="600" data-height="941">
                      <img src="images_cms/museum/posters/1980s/41.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="947">
                      <img src="images_cms/museum/posters/1980s/42.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="933">
                      <img src="images_cms/museum/posters/1980s/43.jpg">
                    </div>
                    <!-- <div class="item" data-width="600" data-height="941">
                      <img src="images_cms/museum/posters/1980s/44.jpg">
                    </div> -->
                    <div class="item" data-width="600" data-height="947">
                      <img src="images_cms/museum/posters/1980s/45.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="947">
                      <img src="images_cms/museum/posters/1980s/46.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="852">
                      <img src="images_cms/museum/posters/1980s/47.jpg">
                    </div>
                  </div>
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-4" class="page-museum-details-item">

                <div class="scroll-target" data-value="1990s"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>1990s</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="posters-masonry-container-04" class="posters-masonry-container-04 masonry-container">
                    <div class="item" data-width="600" data-height="848">
                      <img src="images_cms/museum/posters/1990s/01.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="881">
                      <img src="images_cms/museum/posters/1990s/02.jpg">
                    </div>                  
                    <div class="item" data-width="600" data-height="833">
                      <img src="images_cms/museum/posters/1990s/03.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="834">
                      <img src="images_cms/museum/posters/1990s/04.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="834">
                      <img src="images_cms/museum/posters/1990s/05.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="834">
                      <img src="images_cms/museum/posters/1990s/06.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="850">
                      <img src="images_cms/museum/posters/1990s/07.jpg">
                    </div>                    
                  </div>                  
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-5" class="page-museum-details-item">

                <div class="scroll-target" data-value="2000s"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>2000s</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="posters-masonry-container-05" class="posters-masonry-container-05 masonry-container">
                    <div class="item" data-width="600" data-height="820">
                      <img src="images_cms/museum/posters/2000s/01.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="798">
                      <img src="images_cms/museum/posters/2000s/02.jpg">
                    </div>                  
                    <div class="item" data-width="600" data-height="851">
                      <img src="images_cms/museum/posters/2000s/03.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="849">
                      <img src="images_cms/museum/posters/2000s/04.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="802">
                      <img src="images_cms/museum/posters/2000s/05.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="803">
                      <img src="images_cms/museum/posters/2000s/06.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="804">
                      <img src="images_cms/museum/posters/2000s/07.jpg">
                    </div>                    
                    <div class="item" data-width="600" data-height="849">
                      <img src="images_cms/museum/posters/2000s/08.jpg">
                    </div>                                        
                  </div>
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-6" class="page-museum-details-item">

                <div class="scroll-target" data-value="charity"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>Charity Sweep</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="posters-masonry-container-06" class="posters-masonry-container-06 masonry-container">
                    <div class="item" data-width="600" data-height="901">
                      <img src="images_cms/museum/posters/charity-sweep/01.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="901">
                      <img src="images_cms/museum/posters/charity-sweep/02.jpg">
                    </div>                  
                    <div class="item" data-width="600" data-height="851">
                      <img src="images_cms/museum/posters/charity-sweep/03.jpg">
                    </div>
                  </div>                    
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-7" class="page-museum-details-item">

                <div class="scroll-target" data-value="community"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>Community Posters</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="posters-masonry-container-07" class="posters-masonry-container-07 masonry-container">
                    <div class="item" data-width="600" data-height="832">
                      <img src="images_cms/museum/posters/community-posters/01.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="745">
                      <img src="images_cms/museum/posters/community-posters/02.jpg">
                    </div>                  
                    <div class="item" data-width="600" data-height="714">
                      <img src="images_cms/museum/posters/community-posters/03.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="850">
                      <img src="images_cms/museum/posters/community-posters/04.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="851">
                      <img src="images_cms/museum/posters/community-posters/05.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="822">
                      <img src="images_cms/museum/posters/community-posters/06.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="822">
                      <img src="images_cms/museum/posters/community-posters/07.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="824">
                      <img src="images_cms/museum/posters/community-posters/08.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="829">
                      <img src="images_cms/museum/posters/community-posters/09.jpg">
                    </div>
                  </div>
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-7" class="page-museum-details-item">

                <div class="scroll-target" data-value="responsible"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>Responsible Gaming</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="posters-masonry-container-08" class="posters-masonry-container-08 masonry-container">
                    <div class="item" data-width="600" data-height="839">
                      <img src="images_cms/museum/posters/responsible-gaming/01.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="849">
                      <img src="images_cms/museum/posters/responsible-gaming/02.jpg">
                    </div>                  
                    <div class="item" data-width="600" data-height="846">
                      <img src="images_cms/museum/posters/responsible-gaming/03.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="850">
                      <img src="images_cms/museum/posters/responsible-gaming/04.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="848">
                      <img src="images_cms/museum/posters/responsible-gaming/05.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="831">
                      <img src="images_cms/museum/posters/responsible-gaming/06.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="836">
                      <img src="images_cms/museum/posters/responsible-gaming/07.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="849">
                      <img src="images_cms/museum/posters/responsible-gaming/08.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="600">
                      <img src="images_cms/museum/posters/responsible-gaming/09.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="843">
                      <img src="images_cms/museum/posters/responsible-gaming/10.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="824">
                      <img src="images_cms/museum/posters/responsible-gaming/11.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="600">
                      <img src="images_cms/museum/posters/responsible-gaming/12.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="849">
                      <img src="images_cms/museum/posters/responsible-gaming/13.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="848">
                      <img src="images_cms/museum/posters/responsible-gaming/14.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="829">
                      <img src="images_cms/museum/posters/responsible-gaming/15.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="827">
                      <img src="images_cms/museum/posters/responsible-gaming/16.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="852">
                      <img src="images_cms/museum/posters/responsible-gaming/17.jpg">
                    </div>
                  </div>
                </div>

              </div> <!-- end page-museum-details-item -->

            </div>


          </div>
        </div>
      </div>
      
    </div> <!-- #page-wrapper-content -->
  </div> <!-- #page-wrapper -->

  <?php include "includes/footer_desktop.php"; ?>
  <?php include "includes/footer_mobile.php"; ?>

  <?php include "includes/script_museum.php" ?>

</body>
</html>
<?php
// saving captured output to file
file_put_contents('museum-posters.html', ob_get_contents());
// end buffering and displaying page
ob_end_flush();
?>