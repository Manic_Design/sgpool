<?php 
ob_start();
?>
<?php
  global $current_page;
  $current_page = "page-museum-details";
  $page_title = "Museum";
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
  <?php include 'includes/head.php'; ?>
</head>

<body class="<?php echo $current_page; ?>"> <!-- home-expand-header-version -->
  <!-- only the home page has the class home-expand-header-version by default -->

  <?php include "includes/preloader.php"; ?>

  <?php include "includes/header_desktop.php"; ?>
  <?php include "includes/header_mobile.php"; ?>

  <div class="header-desktop-spacer hidden-xs hidden-sm"></div>
  <div class="header-mobile-spacer visible-xs visible-sm"></div>

  <div id="museum-detail-sticky-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h3>Sweep Tickets 1968 – 2017</h3>
          <a href="museum.html" class="back-cta"><span>Back</span></a>
        </div>
      </div>    
    </div>
  </div>

  <div id="page-wrapper">
    <div id="page-wrapper-content">
      
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            
            <div id="page-museum-details-item-container">

              <div id="item-1" class="page-museum-details-item">

                <div class="scroll-target" data-value="1960s"></div>
            
                <div class="page-museum-detail-item-title">
                  <h2>1960s</h2>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, <br class="hidden-xs hidden-sm"> sed do eiusmod tempor.</p> -->
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="sweep-masonry-container-01" class="sweep-masonry-container-01 masonry-container">
                    <div class="item" data-width="1200" data-height="707">
                      <img src="images_cms/museum/sweep/1960s/01.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="709">
                      <img src="images_cms/museum/sweep/1960s/02.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="698">
                      <img src="images_cms/museum/sweep/1960s/03.jpg">
                    </div>
                  </div>                  
                </div>

              </div> <!-- end page-museum-details-item -->
              
              <div id="item-2" class="page-museum-details-item">

                <div class="scroll-target" data-value="1970s"></div>
            
                <div class="page-museum-detail-item-title">
                  <h2>1970s</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="sweep-masonry-container-02" class="sweep-masonry-container-02 masonry-container">
                    <div class="item" data-width="1200" data-height="506">
                      <img src="images_cms/museum/sweep/1970s/01.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="505">
                      <img src="images_cms/museum/sweep/1970s/02.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="509">
                      <img src="images_cms/museum/sweep/1970s/03.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="511">
                      <img src="images_cms/museum/sweep/1970s/04.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="512">
                      <img src="images_cms/museum/sweep/1970s/05.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="512">
                      <img src="images_cms/museum/sweep/1970s/06.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="513">
                      <img src="images_cms/museum/sweep/1970s/07.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="513">
                      <img src="images_cms/museum/sweep/1970s/08.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="513">
                      <img src="images_cms/museum/sweep/1970s/09.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="510">
                      <img src="images_cms/museum/sweep/1970s/10.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="510">
                      <img src="images_cms/museum/sweep/1970s/11.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="510">
                      <img src="images_cms/museum/sweep/1970s/12.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="498">
                      <img src="images_cms/museum/sweep/1970s/13.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="502">
                      <img src="images_cms/museum/sweep/1970s/14.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="486">
                      <img src="images_cms/museum/sweep/1970s/15.jpg">
                    </div>
                  </div>
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-3" class="page-museum-details-item">

                <div class="scroll-target" data-value="1980s"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>1980s</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="sweep-masonry-container-03" class="sweep-masonry-container-03 masonry-container">
                    <div class="item" data-width="1200" data-height="697">
                      <img src="images_cms/museum/sweep/1980s/01.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="514">
                      <img src="images_cms/museum/sweep/1980s/02.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="514">
                      <img src="images_cms/museum/sweep/1980s/03.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="512">
                      <img src="images_cms/museum/sweep/1980s/04.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="513">
                      <img src="images_cms/museum/sweep/1980s/05.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="505">
                      <img src="images_cms/museum/sweep/1980s/06.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="513">
                      <img src="images_cms/museum/sweep/1980s/07.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="512">
                      <img src="images_cms/museum/sweep/1980s/08.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="511">
                      <img src="images_cms/museum/sweep/1980s/09.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="508">
                      <img src="images_cms/museum/sweep/1980s/10.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="505">
                      <img src="images_cms/museum/sweep/1980s/11.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="508">
                      <img src="images_cms/museum/sweep/1980s/12.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="509">
                      <img src="images_cms/museum/sweep/1980s/13.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="691">
                      <img src="images_cms/museum/sweep/1980s/14.jpg">
                    </div>
                  </div>                  
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-4" class="page-museum-details-item">

                <div class="scroll-target" data-value="1990s"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>1990s</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="sweep-masonry-container-04" class="sweep-masonry-container-04 masonry-container">
                    <div class="item" data-width="1200" data-height="762">
                      <img src="images_cms/museum/sweep/1990s/01.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="765">
                      <img src="images_cms/museum/sweep/1990s/02.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="567">
                      <img src="images_cms/museum/sweep/1990s/03.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="692">
                      <img src="images_cms/museum/sweep/1990s/04.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="699">
                      <img src="images_cms/museum/sweep/1990s/05.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="715">
                      <img src="images_cms/museum/sweep/1990s/06.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="535">
                      <img src="images_cms/museum/sweep/1990s/07.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="716">
                      <img src="images_cms/museum/sweep/1990s/08.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="717">
                      <img src="images_cms/museum/sweep/1990s/09.jpg">
                    </div>
                  </div>                  
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-5" class="page-museum-details-item">

                <div class="scroll-target" data-value="2000s"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>2000s</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="sweep-masonry-container-05" class="sweep-masonry-container-05 masonry-container">
                    <div class="item" data-width="1200" data-height="726">
                      <img src="images_cms/museum/sweep/2000s/01.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="722">
                      <img src="images_cms/museum/sweep/2000s/02.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="723">
                      <img src="images_cms/museum/sweep/2000s/03.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="720">
                      <img src="images_cms/museum/sweep/2000s/04.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="719">
                      <img src="images_cms/museum/sweep/2000s/05.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="721">
                      <img src="images_cms/museum/sweep/2000s/06.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="718">
                      <img src="images_cms/museum/sweep/2000s/07.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="603">
                      <img src="images_cms/museum/sweep/2000s/08.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="605">
                      <img src="images_cms/museum/sweep/2000s/09.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="604">
                      <img src="images_cms/museum/sweep/2000s/10.jpg">
                    </div>

                    <div class="item" data-width="1200" data-height="604">
                      <img src="images_cms/museum/sweep/2000s/11.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="604">
                      <img src="images_cms/museum/sweep/2000s/12.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="607">
                      <img src="images_cms/museum/sweep/2000s/13.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="605">
                      <img src="images_cms/museum/sweep/2000s/14.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="606">
                      <img src="images_cms/museum/sweep/2000s/15.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="605">
                      <img src="images_cms/museum/sweep/2000s/16.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="607">
                      <img src="images_cms/museum/sweep/2000s/17.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="605">
                      <img src="images_cms/museum/sweep/2000s/18.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="610">
                      <img src="images_cms/museum/sweep/2000s/19.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="605">
                      <img src="images_cms/museum/sweep/2000s/20.jpg">
                    </div>

                    <div class="item" data-width="1200" data-height="609">
                      <img src="images_cms/museum/sweep/2000s/21.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="606">
                      <img src="images_cms/museum/sweep/2000s/22.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="1263">
                      <img src="images_cms/museum/sweep/2000s/23.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="1271">
                      <img src="images_cms/museum/sweep/2000s/24.jpg">
                    </div>
                    <div class="item" data-width="600" data-height="1266">
                      <img src="images_cms/museum/sweep/2000s/25.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="591">
                      <img src="images_cms/museum/sweep/2000s/26.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="594">
                      <img src="images_cms/museum/sweep/2000s/27.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="599">
                      <img src="images_cms/museum/sweep/2000s/28.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="594">
                      <img src="images_cms/museum/sweep/2000s/29.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="594">
                      <img src="images_cms/museum/sweep/2000s/30.jpg">
                    </div>

                    <div class="item" data-width="1200" data-height="596">
                      <img src="images_cms/museum/sweep/2000s/31.jpg">
                    </div>
                  </div>                  
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-6" class="page-museum-details-item">

                <div class="scroll-target" data-value="2010s"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>2010s</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="sweep-masonry-container-06" class="sweep-masonry-container-06 masonry-container">
                    <div class="item" data-width="1200" data-height="579">
                      <img src="images_cms/museum/sweep/2010s/01.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="598">
                      <img src="images_cms/museum/sweep/2010s/02.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="601">
                      <img src="images_cms/museum/sweep/2010s/03.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="595">
                      <img src="images_cms/museum/sweep/2010s/04.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="596">
                      <img src="images_cms/museum/sweep/2010s/05.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="598">
                      <img src="images_cms/museum/sweep/2010s/06.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="598">
                      <img src="images_cms/museum/sweep/2010s/07.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="597">
                      <img src="images_cms/museum/sweep/2010s/08.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="596">
                      <img src="images_cms/museum/sweep/2010s/09.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="600">
                      <img src="images_cms/museum/sweep/2010s/10.jpg">
                    </div>

                    <div class="item" data-width="1200" data-height="596">
                      <img src="images_cms/museum/sweep/2010s/11.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="598">
                      <img src="images_cms/museum/sweep/2010s/12.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="598">
                      <img src="images_cms/museum/sweep/2010s/13.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="601">
                      <img src="images_cms/museum/sweep/2010s/14.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="584">
                      <img src="images_cms/museum/sweep/2010s/15.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="593">
                      <img src="images_cms/museum/sweep/2010s/16.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="599">
                      <img src="images_cms/museum/sweep/2010s/17.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="590">
                      <img src="images_cms/museum/sweep/2010s/18.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="581">
                      <img src="images_cms/museum/sweep/2010s/19.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="587">
                      <img src="images_cms/museum/sweep/2010s/20.jpg">
                    </div>

                    <div class="item" data-width="1200" data-height="596">
                      <img src="images_cms/museum/sweep/2010s/21.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="590">
                      <img src="images_cms/museum/sweep/2010s/22.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="586">
                      <img src="images_cms/museum/sweep/2010s/23.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="592">
                      <img src="images_cms/museum/sweep/2010s/24.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="594">
                      <img src="images_cms/museum/sweep/2010s/25.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="592">
                      <img src="images_cms/museum/sweep/2010s/26.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="592">
                      <img src="images_cms/museum/sweep/2010s/27.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="590">
                      <img src="images_cms/museum/sweep/2010s/28.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="592">
                      <img src="images_cms/museum/sweep/2010s/29.jpg">
                    </div>
                  </div>                  
                </div>

              </div> <!-- end page-museum-details-item -->

              <div id="item-7" class="page-museum-details-item">

                <div class="scroll-target" data-value="Charity"></div>
                                
                <div class="page-museum-detail-item-title">
                  <h2>The Charity Sweep</h2>
                </div>

                <div class="page-museum-detail-item-content">
                  <div id="sweep-masonry-container-07" class="sweep-masonry-container-07 masonry-container">
                    <div class="item" data-width="1200" data-height="693">
                      <img src="images_cms/museum/sweep/charity-sweep/01.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="695">
                      <img src="images_cms/museum/sweep/charity-sweep/02.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="694">
                      <img src="images_cms/museum/sweep/charity-sweep/03.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="700">
                      <img src="images_cms/museum/sweep/charity-sweep/04.jpg">
                    </div>
                    <div class="item" data-width="1200" data-height="716">
                      <img src="images_cms/museum/sweep/charity-sweep/05.jpg">
                    </div>
                  </div>                  
                </div>

              </div> <!-- end page-museum-details-item -->

            </div>


          </div>
        </div>
      </div>
      
    </div> <!-- #page-wrapper-content -->
  </div> <!-- #page-wrapper -->

  <?php include "includes/footer_desktop.php"; ?>
  <?php include "includes/footer_mobile.php"; ?>

  <?php include "includes/script_museum.php" ?>

</body>
</html>
<?php
// saving captured output to file
file_put_contents('museum-sweep.html', ob_get_contents());
// end buffering and displaying page
ob_end_flush();
?>