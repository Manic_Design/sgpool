<?php 
ob_start();
?>
<?php
  global $current_page;
  $current_page = "page-milestones-org";

?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
  <?php include 'includes/head.php'; ?>
</head>

<body class="<?php echo $current_page; ?>"> <!-- home-expand-header-version -->
  <!-- only the home page has the class home-expand-header-version by default -->

  <?php include "includes/preloader.php"; ?>

  <?php include "includes/header_desktop.php"; ?>
  <?php include "includes/header_mobile.php"; ?>

  <div class="header-desktop-spacer hidden-xs hidden-sm"></div>
  <div class="header-mobile-spacer visible-xs visible-sm"></div>

  <div id="page-wrapper">
    <div id="page-wrapper-content">   
        
        <div id="page-milestones-indicators-container">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-1">
                <div id="start-sticky-trigger"></div>
                
                <div class="wrap">
                  <div id="page-milestones-indicators" class="page-indicators">
                    <ul id="page-milestones-indicators-ul">
                        <li id="content-group-indicator-1" class="content-gorup-indicator">
                          <a href="javascript:void(0);" class="indicator-links">1960s</a>
                          <ul>
                            <li><a href="#1968">1968</a></li>
                            <li><a href="#1969">1969</a></li>
                          </ul>
                        </li>
                        <li id="content-group-indicator-2" class="content-gorup-indicator">
                          <a href="javascript:void(0);" class="indicator-links">1970s</a>
                          <ul>
                            <li><a href="#1972">1972</a></li>
                            <li><a href="#1973">1973</a></li>
                          </ul>
                        </li>
                        <li id="content-group-indicator-3" class="content-gorup-indicator">
                          <a href="javascript:void(0);" class="indicator-links">1980s</a>
                          <ul>
                            <li><a href="#1980">1980</a></li>
                            <li><a href="#1986">1986</a></li>
                            <li><a href="#1988">1988</a></li>
                          </ul>
                        </li>
                        <li id="content-group-indicator-4" class="content-gorup-indicator">
                          <a href="javascript:void(0);" class="indicator-links">1990s</a>
                          <ul>
                            <li><a href="#1991">1991</a></li>
                            <li><a href="#1992">1992</a></li>
                            <li><a href="#1993">1993</a></li>
                            <li><a href="#1999">1999</a></li>
                          </ul>
                        </li>
                        <li id="content-group-indicator-5" class="content-gorup-indicator">
                          <a href="javascript:void(0);" class="indicator-links">2000s</a>
                          <ul>
                            <li><a href="#2000">2000</a></li>
                            <li><a href="#2002">2002</a></li>
                            <li><a href="#2003">2003</a></li>
                            <li><a href="#2004">2004</a></li>
                            <li><a href="#2005">2005</a></li>
                            <li><a href="#2006">2006</a></li>
                            <li><a href="#2008">2008</a></li>
                          </ul>
                        </li>
                        <li id="content-group-indicator-6" class="content-gorup-indicator">
                          <a href="javascript:void(0);" class="indicator-links">2010s</a>
                          <ul>
                            <li><a href="#2010">2010</a></li>
                            <li><a href="#2011">2011</a></li>
                            <li><a href="#2012">2012</a></li>
                          </ul>
                        </li>
                      </ul>  
                  </div>
                </div>
              </div>     
            </div>
          </div>        
        </div>

        <article id="page-milestones-container"> 

          <div id="page-milestones-seperator-line-container">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-1 col-md-push-1">
                  <div id="page-milestones-seperator-line"></div>
                </div>
              </div>
            </div>
          </div>        

          <div class="container-fluid">
            
            <!-- <div class="row">               -->

              <!-- <div class="col-md-1"></div> -->
              
              <!-- <div class="col-md-11">                 -->                

                <div id="page-milestones-content" class="row">                                  
                                    
                  <div id="content-group-1" class="page-content-group">
                    <div class="col-md-4 col-md-offset-2">
                      <div id="trigger-content-group-1" class="scroll-target" data-value="1960s"></div>
                      <div id="item-1" class="floating-item"> 
                        <div class="scroll-target" data-value="1968"></div>
                        <div class="text-container">
                          <h2>1968 <br class="hidden-xs hidden-sm">Founded</h2>
                          <p>Introduced TOTO which was sold at standalone booths.</p>
                        </div>                                         
                        <div class="manic-image-container full-col-width-version">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-1968.jpg"
                            data-image-tablet="images_cms/milestones-org/year-1968-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-1968-mobile.jpg" alt="">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 col-md-offset-1">                      
                      <div id="item-2" class="floating-item">
                        <div class="scroll-target" data-value="1969"></div>
                        <div class="text-container">              
                          <h2>1969 <br class="hidden-xs hidden-sm">Singapore Sweep</h2>
                          <p>Introduced Singapore Sweep with $400,000 as the first prize.</p>
                        </div>
                        <div class="manic-image-container full-col-width-version">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-1969.jpg"
                            data-image-tablet="images_cms/milestones-org/year-1969-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-1969-mobile.jpg" alt="">
                        </div>
                      </div>
                    </div>
                  </div> <!-- end content-group-1 -->
                  
                  <div id="content-group-2" class="page-content-group">
                    <div class="col-md-4 col-md-offset-3">
                      <div id="trigger-content-group-2" class="scroll-target" data-value="1970s"></div>
                      <div id="item-3" class="floating-item">                  
                        <div class="scroll-target" data-value="1972"></div>
                        <div class="text-container">
                          <h2 id="test">1972 <br class="hidden-xs hidden-sm">Colombo Court Office</h2>
                          <p>Moved from first office in Percival Road to Columbo Court.</p>
                        </div>
                        <div class="manic-image-container full-col-width-version">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-1972.jpg"
                            data-image-tablet="images_cms/milestones-org/year-1972-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-1972-mobile.jpg" alt="">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-11 col-md-offset-1">
                      <div id="item-4" class="floating-item longest-height-version">        
                        <div class="scroll-target" data-value="1973"></div>
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-1973.jpg"
                            data-image-tablet="images_cms/milestones-org/year-1973-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-1973-mobile.jpg" alt="">
                        </div>
                        <div class="text-container absolute-version">
                          <h2 id="test2">1973 <br class="hidden-xs hidden-sm">National Stadium</h2>
                          <p>Contributed S$14.5 million to its construction cost.</p>
                        </div>
                      </div>
                    </div>
                  </div> <!-- end content-group-2 -->
                  
                  <div id="content-group-3" class="page-content-group">
                    <div class="col-md-4 col-md-offset-2">
                      <div id="trigger-content-group-3" class="scroll-target" data-value="1980s"></div>
                      <div id="item-5" class="floating-item">
                        <div class="scroll-target" data-value="1980"></div>
                        <div class="text-container">
                          <h2>1980 <br class="hidden-xs hidden-sm">Temasek Holdings</h2>
                          <p>Became subsidiary of Temasek Holdings.</p>                        
                        </div>                  
                      </div>
                      <div id="item-6" class="floating-item">     
                        <div class="scroll-target" data-value="1986"></div>
                        <div class="text-container">
                          <h2>1986 <br class="hidden-xs hidden-sm">4D + Computerisation Drive</h2>
                          <p>Launched 4-Digit number game and moved from manual operations to computerised gaming system.</p>
                        </div>             
                        <div class="manic-image-container full-col-width-version">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-1986.jpg"
                            data-image-tablet="images_cms/milestones-org/year-1986-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-1986-mobile.jpg" alt="">
                        </div>
                      </div>
                      <div id="item-7" class="floating-item long-verison">
                        <div class="manic-image-container full-col-width-version-2">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-1986-02.jpg"
                            data-image-tablet="images_cms/milestones-org/year-1986-02-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-1986-02-mobile.jpg" alt="">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-5 col-md-offset-1">
                      <div id="item-8" class="floating-item">
                        <div class="scroll-target" data-value="1988"></div>
                        <div class="text-container">
                          <h2>1988 <br class="hidden-xs hidden-sm">Singapore Indoor Stadium</h2>
                          <p>Sponsored S$45 million to its construction cost.</p>
                        </div>                                        
                      </div>                      
                      <div id="item-9" class="floating-item long-height-version">
                        <div class="scroll-target" data-value="1988"></div>
                        <div class="text-container">
                          <h2>International Plaza</h2>
                          <p>Moved office to International Plaza.</p>
                        </div>                
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-1988.jpg"
                            data-image-tablet="images_cms/milestones-org/year-1988-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-1988-mobile.jpg" alt="">
                        </div>
                      </div>                      
                    </div>
                  </div> <!-- end content-group-3 -->

                  <div id="content-group-4" class="page-content-group">
                    <div class="col-md-11 col-md-offset-1">
                      <div id="trigger-content-group-4" class="scroll-target" data-value="1990s"></div>
                      <div id="item-10" class="floating-item longest-height-version">
                        <div class="scroll-target" data-value="1991"></div>
                        <div class="text-container absolute-version">
                          <h2>1991 <br class="hidden-xs hidden-sm">National Day Parade</h2>
                          <p>Supported the National Day Parade since.</p>
                        </div>            
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-1991.jpg"
                            data-image-tablet="images_cms/milestones-org/year-1991-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-1991-mobile.jpg" alt="">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 col-md-offset-2">
                      <div id="item-11" class="floating-item">
                        <div class="scroll-target" data-value="1992"></div>
                        <div class="text-container">
                          <h2>1992 <br class="hidden-xs hidden-sm">4D Sweep</h2>
                          <p>Inaugurated 4D-Sweep, a 4D game based on the Singapore Sweep draw results.</p>
                        </div>             
                        <div class="manic-image-container full-col-width-version">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-1992.jpg"
                            data-image-tablet="images_cms/milestones-org/year-1992-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-1992-mobile.jpg" alt="">
                        </div>
                      </div>                      
                    </div>
                    
                    <div class="col-md-4 col-md-offset-1">
                      <div id="item-12" class="floating-item">
                        <div class="scroll-target" data-value="1993"></div> 
                        <div class="text-container">                             
                          <h2>1993 <br class="hidden-xs hidden-sm">NCSS Platinum Award</h2>
                          <p>Awarded with NCSS Platinum Award for contributions to the Sports Excellence 2000 (SPEX 2000) programme.</p>                          
                        </div>
                      </div>
                      <div id="item-13" class="floating-item">
                        <div class="text-container">                             
                          <h2>SPEX 2000</h2>
                          <p>Supported the development and nurturing of sports talents in Singapore.</p>                          
                        </div>
                      </div>
                      <div id="item-14" class="floating-item">  
                        <div class="scroll-target" data-value="1999"></div> 
                        <div class="text-container">                                        
                          <h2>1999 <br class="hidden-xs hidden-sm">Football Betting</h2>
                          <p>Launched football betting on the S. League in support of professional football development in Singapore.</p>
                        </div>
                      </div>
                    </div>                    
                  </div> <!-- end content-group-4 -->

                  <div id="content-group-5" class="page-content-group">
                    <div class="col-md-6 col-md-offset-3">
                      <div id="trigger-content-group-5" class="scroll-target" data-value="2000s"></div>
                      <div id="item-15" class="floating-item">
                        <div id="trigger-content-5" class="scroll-target" data-value="2000"></div> 
                        <div class="text-container">                                  
                          <h2>2000 <br class="hidden-xs hidden-sm">Selegie Road</h2>
                          <p>Moved office from Paradiz Centre at Selegie Road.</p>
                        </div>
                      </div>
                      <div id="item-16" class="floating-item long-height-version">                                         
                        <div class="text-container">
                          <h2>TOTO Hongbao Draw</h2>
                          <p>Conducted the first TOTO Hongbao Draw, reaching a jackpot prize of $11 million. </p>
                        </div>
                        <div class="manic-image-container full-col-width-version">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2000.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2000-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2000-mobile.jpg" alt="">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 col-md-offset-2">
                      <div id="item-17" class="floating-item">
                      <div id="trigger-content-6" class="scroll-target" data-value="2002"></div>                         
                        <div class="text-container">
                          <h2>2002 <br class="hidden-xs hidden-sm">International Football Betting</h2>
                          <p>Introduced football betting for international matches.</p>                          
                        </div>
                      </div>
                      <div id="item-18" class="floating-item long-height-version">                        
                        <div class="text-container">
                          <h2>Esplanade</h2>
                          <p>Funded the construction of Esplanade - Theatres on the Bay.</p>
                        </div>
                        <div class="manic-image-container full-col-width-version-2">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2002.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2002-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2002-mobile.jpg" alt="">
                        </div>
                      </div>                      
                    </div>
                    <div class="col-md-5 col-md-offset-1">
                      <div id="item-19" class="floating-item long-height-version">
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2002-02.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2002-02-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2002-02-mobile.jpg" alt="">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12"></div>

                    <div class="col-md-3 col-md-offset-2">
                      <div id="item-20" class="floating-item">
                        <div class="scroll-target" data-value="2003"></div> 
                        <div class="text-container">
                          <h2>2003 <br class="hidden-xs hidden-sm">iShine</h2>
                          <p>Launched iShine, the staff community programme.</p>
                        </div>
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2003.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2003-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2003-mobile.jpg" alt="">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div id="item-21" class="floating-item">
                        <div class="scroll-target" data-value="2004"></div> 
                        <div class="text-container">
                          <h2>2004 <br class="hidden-xs hidden-sm">Tote Board</h2>
                          <p>Became a subsidiary of Tote Board, took over Singapore Turf Club branches and started conducting 4D Draws.</p>
                        </div>
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2004.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2004-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2004-mobile.jpg" alt="">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                      <div id="item-22" class="floating-item">
                        <div class="scroll-target" data-value="2005"></div> 
                        <div class="text-container">
                          <h2>2005 <br class="hidden-xs hidden-sm">Phone Betting</h2>
                          <p>Launched telephone betting.</p>
                        </div>
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2005.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2005-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2005-mobile.jpg" alt="">
                        </div>
                      </div>
                      <div id="item-23" class="floating-item">
                        <div class="scroll-target" data-value="2006"></div> 
                        <div class="text-container">
                          <h2>2007 <br class="hidden-xs hidden-sm">Responsible <br> Gaming <br> Programme</h2>
                          <p>Launched the programme to create a responsible gaming environment for all.</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-md-offset-2">
                      <div id="item-24" class="floating-item long-height-version">
                        <div class="scroll-target" data-value="2008"></div> 
                        <div class="text-container">
                          <h2>2008 <br class="hidden-xs hidden-sm">Formula One Betting</h2>
                          <p>Offered sports betting on Formula One races.</p>
                        </div>
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2008.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2008-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2008-mobile.jpg" alt="">                          
                        </div>
                      </div>
                    </div>   
                  </div> <!-- end content-group-5 -->

                  <div class="col-md-12"></div>

                  <div id="content-group-6" class="page-content-group">
                    <div class="col-md-3 col-md-offset-2">
                      <div id="trigger-content-group-6" class="scroll-target" data-value="2010s"></div>
                      <div id="item-25" class="floating-item">
                        <div class="scroll-target" data-value="2010"></div> 
                        <div class="text-container">                                    
                          <h2>2010 <br class="hidden-xs hidden-sm">Livewire</h2>
                          <p>Set up LiveWire betting venues.</p>
                        </div>
                      </div>
                      <div id="item-26" class="floating-item">
                        <div class="scroll-target" data-value="2011"></div>
                        <div class="text-container">
                          <h2>2011 <br class="hidden-xs hidden-sm">Gardens By The Bay</h2>
                          <p>Sponsored the opening of Gardens by the Bay.</p>
                        </div>
                      </div>
                      <div id="item-27" class="floating-item">
                        <div class="scroll-target" data-value="2012"></div>
                        <div class="text-container">
                          <h2>2012 <br class="hidden-xs hidden-sm">Level 4 Accreditation</h2>
                          <p>Achieved Level 4 of World Lottery Association Responsible Gaming Workframe, the highest global standard of responsible gaming.</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div id="item-28" class="floating-item">
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2011.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2011-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2011-mobile.jpg" alt="">   
                        </div>
                      </div>
                    </div>

                    <div class="col-md-11 col-md-offset-1">
                      <div id="item-29" class="floating-item longest-height-version">     
                        <div class="scroll-target" data-value="2013"></div>
                        <div class="text-container absolute-version">
                          <h2>2013 <br class="hidden-xs hidden-sm">Singapore Pools Building</h2>
                          <p>Officially opened the 12-storey Singapore Pools Building at Middle Road on 28 April and it won BCA Green Mark GoldPlus Award for Singapore Pools Building.</p>
                        </div>
                        <div class="manic-image-container">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2013.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2013-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2013-mobile.jpg" alt="">      
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 col-md-offset-2">
                      <div id="item-30" class="floating-item long-height-version">
                        <div class="scroll-target" data-value="2015"></div>
                        <div class="manic-image-container full-col-width-version-2">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2015.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2015-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2015-mobile.jpg" alt="">      
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 col-md-offset-1">
                      <div id="item-31" class="floating-item long-height-version">
                        <div class="text-container">
                          <h2>2015 <br class="hidden-xs hidden-sm">National Art Gallery</h2>
                          <p>Sponsored the restoration and opening of the National Art Gallery.</p>                    
                        </div>
                        <div class="manic-image-container full-col-width-version-2">                          
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2015-02.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2015-02-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2015-02-mobile.jpg" alt="">      
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 col-md-offset-2">
                      <div id="item-32" class="floating-item">
                        <div class="scroll-target" data-value="2016"></div>
                        <div class="text-container">
                          <h2>2016 <br class="hidden-xs hidden-sm">World Lottery Summit</h2>
                          <p>Co-hosted the World Lottery Summit from 6 to 9 Nov, the second time in 10 years.</p>
                        </div>
                        <div class="manic-image-container full-col-width-version">
                          <img src="" 
                            data-image-desktop="images_cms/milestones-org/year-2016.jpg"
                            data-image-tablet="images_cms/milestones-org/year-2016-tablet.jpg"
                            data-image-mobile="images_cms/milestones-org/year-2016-mobile.jpg" alt="">      
                        </div>
                      </div>
                    </div>
                  </div><!-- end content-group-6 -->                              

                </div> <!-- end page-milestones-content -->

              <!-- </div> -->

              <div id="end-sticky-trigger" class="col-md-12"></div>    

            <!-- </div> --> <!-- end row -->

          </div>

        </article> <!-- end page-milestones-content-container -->            

      </div> <!-- #page-wrapper-content -->
  </div> <!-- #page-wrapper -->

  <?php include "includes/footer_desktop.php"; ?>
  <?php include "includes/footer_mobile.php"; ?>

  <?php include "includes/script_milestones_org.php" ?>

</body>
</html>
<?php
// saving captured output to file
file_put_contents('milestones-org.html', ob_get_contents());
// end buffering and displaying page
ob_end_flush();
?>