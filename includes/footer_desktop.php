  <!--
     _____ ___   ___ _____ _____ ____
    |  ___/ _ \ / _ \_   _| ____|  _ \
    | |_ | | | | | | || | |  _| | |_) |
    |  _|| |_| | |_| || | | |___|  _ <
    |_|   \___/ \___/ |_| |_____|_| \_\

  -->
  <div id="footer-links-desktop" class="hidden-xs hidden-sm">
    <div class="container-fluid">
      
      <div class="row">
        <div class="col-md-3">
          <div id="footer-desktop-logo-container">
            <div id="footer-desktop-logo"></div>
          </div>
        </div>
        <div class="col-md-1">          
          <h5><a href="events.html">Events</a></h5>          
        </div>
        <div class="col-md-1 col-md-offset-1">
          <h5>Milestones</h5>
          <ul>
            <li><a href="milestones.html#1968">1960s</a></li>
            <li><a href="milestones.html#1972">1970s</a></li>
            <li><a href="milestones.html#1980">1980s</a></li>
            <li><a href="milestones.html#1991">1990s</a></li>
            <li><a href="milestones.html#2000">2000s</a></li>
            <li><a href="milestones.html#2010">2010s</a></li>
          </ul>
        </div>
        <div class="col-md-2 col-lg-1 col-md-offset-1">
          <h5>Museum</h5>
          <ul>
            <li><a href="museum-sweep.html">Sweep Tickets</a></li>
            <li><a href="museum-posters.html">Posters</a></li>
            <li><a href="museum-bet-slips-tickets.html">Bet Slips & Tickets</a></li>
            <li><a href="museum-retail-outlets.html">Retail Outlets</a></li>
            <li><a href="museum-lottery-draws.html">Lottery Draws</a></li>
          </ul>
        </div>
        <div class="col-md-2 col-md-offset-1">
          <h5>Singapore Pools (Private) Limited</h5>
          <p>210 Middle Road, #01-01, Singapore Pools Building, <br>
          Singapore 188994 <br>
          <a href="tel:+65 6216 8168">(65) 6216 8168</a><br>
          <a href="mailto:CorporateComms@sgpoolz.com.sg">CorporateComms@sgpoolz.com.sg</a></p>
        </div>        
      </div>

    </div>
  </div>

  <footer id="footer-desktop" class="hidden-xs hidden-sm">
    <div class="container-fluid">
      
      <div class="row">
        <div class="col-md-6">

          <div id="footer-copyright-and-links-container">
            <div class="row">
              <div class="col-md-12">
                <p id="footer-copyright-container">All rights reserved &copy; Singapore Pools 2018</p>
                <ul id="footer-links-container">
                  <li><a href="http://www.singaporepools.com.sg/en/rules/Pages/website-tnc.html">Terms & Conditions</a></li>
                </ul>
              </div>
            </div>
          </div>
          
        </div>
        <div class="col-md-6">

          <div id="footer-social-media">
            <ul>
              <li><a href="https://www.facebook.com/SingaporePoolsInTheCommunity" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="https://www.instagram.com/sgpools/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li><a href="https://www.youtube.com/channel/UCCzL1gEhcKBOOLPGNIbh9-w" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>              
            </ul>
          </div> <!- - footer-legal - ->

        </div>        
      </div> <!-- row -->

    </div>
  </footer> <!-- footer-desktop -->
